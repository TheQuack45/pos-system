﻿using PosSystem.Model;
using PosSystem.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PosSystem.ViewModel
{
    public class MainWindowViewModel : BindableBase
    {
        #region Members definition
        #region Properties definition
        private ObservableCollection<ItemCategory> _itemCategories;
        public ObservableCollection<ItemCategory> ItemCategories
        {
            get { return this._itemCategories; }
            private set { this.SetProperty(ref this._itemCategories, value); }
        }
        private ItemCategory _selectedCategory;
        public ItemCategory SelectedCategory
        {
            get { return this._selectedCategory; }
            set { this.SetProperty<ItemCategory>(ref this._selectedCategory, value); }
        }

        // TODO: The ViewModel maybe shouldn't have a Model instance inside it? Not sure.
        private Order _currentOrder;
        public Order CurrentOrder
        {
            get { return this._currentOrder; }
            set { this.SetProperty<Order>(ref this._currentOrder, value); }
        }

        // TODO: Add support for available item listings.
        // This property should be filtering the factory's available items based on the type set by SelectedCategory.
        private ObservableCollection<MealPrototype> _availableItems;
        public ObservableCollection<MealPrototype> AvailableItems
        {
            get;
            set;
        }

        public decimal Subtotal
        {
            get { return this.CurrentOrder.RawPrice; }
        }

        public decimal Tax
        {
            get { return this.CurrentOrder.TaxCost; }
        }

        public decimal Total
        {
            get { return this.CurrentOrder.TotalPrice; }
        }
        #endregion Properties definition

        #region Fields definition
        private readonly IMealFactory _mealFactory;
        #endregion Fields definition

        #region ICommand members definition
        //private ICommand _addItemCommand;
        //public ICommand AddItemCommand
        //{
        //    get
        //    {
        //        if (this._addItemCommand == null)
        //        { this._addItemCommand = new RelayCommand(() => this.AddItem()); }
        //        return this._addItemCommand;
        //    }
        //}
        #endregion ICommand members definition
        #endregion Members definition

        #region Constructors definition
        public MainWindowViewModel()
        {
            this.ItemCategories = new ObservableCollection<ItemCategory>();
            this._mealFactory = new MealFactory();

            this.CurrentOrder = new Order("John Smith", "8675309");
        }
        #endregion Constructors definition

        #region Methods definition
        //public void AddItem()
        //{
        //    var meal = this._mealFactory.CreateMeal("Clams", MealPrototype.MEAL_TYPE.Plate, null);
        //    this.CurrentOrder.AddItem(meal);
        //}
        #endregion Methods definition
    }
}
