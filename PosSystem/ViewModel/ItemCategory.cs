﻿using PosSystem.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.ViewModel
{
    public class ItemCategory : BindableBase
    {
        private string _name;
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty<string>(ref this._name, value); }
        }

        private Model.MealPrototype.MEAL_TYPE _type;
        public Model.MealPrototype.MEAL_TYPE Type
        {
            get { return this._type; }
            set { this.SetProperty<Model.MealPrototype.MEAL_TYPE>(ref this._type, value); }
        }
    }
}
