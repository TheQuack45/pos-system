﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a prototype for all meals, containing possible information that will be made concrete when a Meal is instantiated.
    /// </summary>
    public class MealPrototype
    {
        #region Members definition
        #region Static members definition
        public enum MEAL_TYPE { Plate, Basket, Side, Beverage, Dessert, Combo };
        #endregion Static members definition

        // TODO: Immutable?
        public string Name { get; private set; }
        public List<MealSize> Sizes { get; private set; }
        public MEAL_TYPE Type { get; private set; }
        #endregion Members definition

        #region Constructors definition
        public MealPrototype(string name, MEAL_TYPE type, List<MealSize> sizes)
        {
            this.Name = name;
            this.Type = type;

            // Meals can have multiple sizes. These sizes, along with their prices, are captured in this.Sizes.
            // Each size name must be distinct. For example, there cannot be two different "Small" sizes with different prices.
            if (sizes.Distinct(new SizeSameNameComparer()).Count() != sizes.Count)
                { throw new ArgumentException("There cannot be any duplicate sizes in the given list of sizes."); }
            this.Sizes = sizes;
        }
        #endregion Constructors definition
    }

    /// <summary>
    /// Represents a comparer that checks if two MealSizes are equal, as determined by their Names.
    /// </summary>
    public class SizeSameNameComparer : IEqualityComparer<MealSize>
    {
        public bool Equals(MealSize x, MealSize y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(MealSize size)
        {
            return size.Name.GetHashCode();
        }
    }
}
