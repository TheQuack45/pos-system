﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents the interface for a factory to create Meal objects.
    /// </summary>
    public interface IMealFactory
    {
        Meal CreateMeal(string mealName, MealPrototype.MEAL_TYPE type, string sizeName);
        Meal CreateMeal(MealPrototype prototype, string sizeName);
    }

    /// <summary>
    /// Represents an implementation of a meal factory based on the IMealFactory interface.
    /// </summary>
    public class MealFactory : IMealFactory
    {
        #region Members definition
        #region Static members definition
        private const string MEALS_FILE = "meals.tsv";
        private const string DEFAULT_SIZE_NAME = "Standard";
        private const char TOKENS_SEPARATOR = '\t';
        private const char SIZE_TOKENS_SEPARATOR = ';';
        private const char SIZE_INFO_SEPARATOR = ':';
        #endregion Static members definition

        #region Fields definition
        private readonly List<MealPrototype> _prototypes;
        #endregion Fields definition
        #endregion Members definition

        #region Constructors definition
        public MealFactory()
        {
            string mealsFilePath = Path.Combine(Environment.CurrentDirectory, MEALS_FILE);
            this._prototypes = this.GetPrototypes(mealsFilePath);
        }
        #endregion Constructors definition

        #region Methods definition
        // TODO: Convert prototype file to a JSON rather than TSV.
        /// <summary>
        /// Generates a List of MealPrototypes by reading from the file at the given path and parsing each into a MealPrototype object.
        /// </summary>
        /// <param name="mealsFilePath">Path to the file to read prototype information from.</param>
        /// <returns>List of MealPrototype objects.</returns>
        private List<MealPrototype> GetPrototypes(string mealsFilePath)
        {
            if (mealsFilePath == null)
                { throw new ArgumentNullException("The given meals file name cannot be null.", nameof(mealsFilePath)); }
            if (mealsFilePath == "")
                { throw new ArgumentException("The given meals file name cannot be empty.", nameof(mealsFilePath)); }

            using (var reader = new StreamReader(mealsFilePath))
            {
                List<MealPrototype> prototypes = new List<MealPrototype>();
                string text = reader.ReadToEnd();
                string[] lines = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                
                foreach (string line in lines)
                {
                    string[] tokens = line.Split('\t');

                    // TODO: This error handling needs some work.
                    if (tokens.Length < 3 ||
                        String.IsNullOrEmpty(tokens[0]) ||
                        String.IsNullOrEmpty(tokens[1]) ||
                        String.IsNullOrEmpty(tokens[2]))
                    {
                        // TODO: Invalid line. Log this.
                        continue;
                    }

                    MealPrototype.MEAL_TYPE type = this.ConvertToMealType(tokens[0]);
                    string mealName = tokens[1];
                    List<MealSize> sizes = new List<MealSize>();
                    if (decimal.TryParse(tokens[2], out decimal singlePrice))
                    {
                        // There is only one size, and a name for that size wasn't specified. Use the default name.
                        sizes.Add(new MealSize(DEFAULT_SIZE_NAME, singlePrice));
                    }
                    else
                    {
                        // This item has one or more sizes with different prices, and a name specified for each.
                        // The final token is formatted as such: [sizeName]:[sizePrice];...
                        // The token for each size is formatted as such: [sizeName]:[sizePrice].
                        string sizesToken = tokens[2];
                        sizes = this.ParseMealSizes(sizesToken);
                    }

                    prototypes.Add(new MealPrototype(mealName,
                                                     type,
                                                     sizes));
                }

                return prototypes;
            }
        }

        /// <summary>
        /// Parses the given meal sizes string into a List of MealSize objects.
        /// </summary>
        /// <param name="sizesToken">The string defining meal size info.</param>
        /// <returns>List of MealSize objects.</returns>
        private List<MealSize> ParseMealSizes(string sizesToken)
        {
            List<MealSize> sizes = new List<MealSize>();
            string[] sizeTokens = sizesToken.Split(SIZE_TOKENS_SEPARATOR);

            foreach (string sizeToken in sizeTokens)
            {
                try
                {
                    sizes.Add(this.ParseMealSize(sizeToken));
                }
                catch (Exception e) when (e is ArgumentNullException ||
                                          e is ArgumentException)
                {
                    // Invalid size token.
                    // TODO: Log this.
                    continue;
                }
            }

            return sizes;
        }

        /// <summary>
        /// Parses the given size info token into a MealSize object.
        /// </summary>
        /// <param name="token">Size info token.</param>
        /// <returns>MealSize object populated with info from token.</returns>
        private MealSize ParseMealSize(string token)
        {
            string[] sizeInfo = token.Split(SIZE_INFO_SEPARATOR);

            string sizeName = sizeInfo[0];
            if (string.IsNullOrEmpty(sizeName))
                { throw new ArgumentNullException("The given token did not have a valid size name value.", nameof(token)); }

            if (!decimal.TryParse(sizeInfo[1], out decimal sizePrice))
                { throw new ArgumentException("The given token did not have a valid size price value.", nameof(token)); }

            return new MealSize(sizeName, sizePrice);
        }

        /// <summary>
        /// Converts the given meal type string into a MealPrototype.MEAL_TYPE value.
        /// </summary>
        /// <param name="typeStr">Meal type string.</param>
        /// <returns>Relevant MealPrototype.MEAL_TYPE value.</returns>
        private MealPrototype.MEAL_TYPE ConvertToMealType(string typeStr)
        {
            switch (typeStr)
            {
                case "Plate":
                    return MealPrototype.MEAL_TYPE.Plate;
                case "Basket":
                    return MealPrototype.MEAL_TYPE.Basket;
                case "Side":
                    return MealPrototype.MEAL_TYPE.Side;
                case "Beverage":
                    return MealPrototype.MEAL_TYPE.Beverage;
                case "Dessert":
                    return MealPrototype.MEAL_TYPE.Dessert;
                default:
                    throw new ArgumentException("The given string was not a valid meal type.", nameof(typeStr));
            }
        }

        /// <summary>
        /// Creates an instance of a meal object based on the given name and type, in the size specified by the size name.
        /// </summary>
        /// <param name="mealName">Name of the meal to create.</param>
        /// <param name="type">Type of the meal to create.</param>
        /// <param name="sizeName">Name of the size of the meal to create.</param>
        /// <returns>Meal object based on the given filters.</returns>
        public Meal CreateMeal(string mealName, MealPrototype.MEAL_TYPE type, string sizeName)
        {
            if (string.IsNullOrEmpty(mealName))
                { throw new ArgumentException("The given meal name cannot be null or empty.", nameof(mealName)); }

            if (string.IsNullOrEmpty(sizeName))
            {
                sizeName = DEFAULT_SIZE_NAME;
            }

            // TODO: Should this be done by type first, or by name first?
            // I think filtering by type first would be faster most of the time.
            IEnumerable<MealPrototype> validPrototypes = this._prototypes.Where(p => p.Type == type)
                                                                         .Where(p => p.Name == mealName);
            if (!validPrototypes.Any())
            {
                // No prototypes matching the filter were found.
                throw new ArgumentException("No prototypes with the given name of the given type were found.");
            }

            MealPrototype prototype;
            try
            {
                prototype = validPrototypes.Single();
            }
            catch (InvalidOperationException e)
            {
                // The LINQ .Single() query found more than one element matching the two filters.
                // This means that the list of prototypes has duplicates and is invalid.
                // TODO: Log this.
                throw new InvalidOperationException("This MealFactory's list of prototypes is invalid and has duplicate elements matching the set filters. The selection is ambiguous.", e);
            }
            catch (ArgumentNullException)
            {
                // The selected prototype was null. The list of prototypes contains a null entry.
                // TODO: Log this, and remove the null entry.
                throw;
            }

            return this.CreateMeal(prototype, sizeName);
        }

        /// <summary>
        /// Creates an instance of a Meal object based on the given prototype and size name.
        /// </summary>
        /// <param name="prototype">The prototype to base the new object on.</param>
        /// <param name="sizeName">The name of the new meal's size.</param>
        /// <returns>An instance of a meal object based on the type of the given prototype.</returns>
        public Meal CreateMeal(MealPrototype prototype, string sizeName)
        {
            if (prototype == null)
                { throw new ArgumentNullException("The given meal prototype cannot be null.", nameof(prototype)); }

            if (string.IsNullOrEmpty(sizeName))
            {
                sizeName = DEFAULT_SIZE_NAME;
            }

            switch (prototype.Type)
            {
                case MealPrototype.MEAL_TYPE.Plate:
                    return new Plate(sizeName, prototype);
                case MealPrototype.MEAL_TYPE.Basket:
                    return new Basket(sizeName, prototype);
                case MealPrototype.MEAL_TYPE.Side:
                    return new Side(sizeName, prototype);
                case MealPrototype.MEAL_TYPE.Beverage:
                    return new Beverage(sizeName, prototype);
                case MealPrototype.MEAL_TYPE.Dessert:
                    return new Dessert(sizeName, prototype);
                case MealPrototype.MEAL_TYPE.Combo:
                    throw new NotImplementedException("Combos are not supported yet.");
                default:
                    throw new ArgumentException("The given meal prototype was not of a valid type.", nameof(prototype));
            }
        }
        #endregion Methods definition
    }
}
