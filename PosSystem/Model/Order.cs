﻿using PosSystem.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    public class Order : BindableBase
    {
        #region Members definition
        #region Static members definition
        private const decimal TAX_RATE = 0.08M;
        #endregion Static members definition

        private string _customerName;
        public string CustomerName
        {
            get { return this._customerName; }
            set { this.SetProperty<string>(ref this._customerName, value); }
        }
        // Not bothering with any custom phone number class. That would be a lot of verification.
        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return this._phoneNumber; }
            set { this.SetProperty<string>(ref this._phoneNumber, value); }
        }
        private ObservableCollection<Meal> _items;
        public ObservableCollection<Meal> Items
        {
            get { return this._items; }
            private set { this.SetProperty<ObservableCollection<Meal>>(ref this._items, value); }
        }

        public decimal RawPrice
        {
            get { return this.GetSubtotal(); }
        }

        public decimal TaxCost
        {
            get { return this.GetTaxCost(); }
        }

        public decimal TotalPrice
        {
            get { return this.GetTotal(); }
        }
        #endregion Members definition

        #region Constructors definition
        public Order()
        {
            this.Items = new ObservableCollection<Meal>();
        }

        public Order(string name, string phoneNumber)
            : this()
        {
            this.CustomerName = name;
            this.PhoneNumber = phoneNumber;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Sums the prices of all this Order's items and returns the total.
        /// </summary>
        /// <returns>The total price for all current items.</returns>
        private decimal GetSubtotal()
        {
            decimal total = 0.0M;

            foreach (Meal item in this.Items)
            {
                total += item.Price;
            }

            return total;
        }

        /// <summary>
        /// Calculates the amount that the customer will pay in tax.
        /// </summary>
        /// <returns>Decimal amount that customer will pay in tax.</returns>
        private decimal GetTaxCost()
        {
            return this.RawPrice * TAX_RATE;
        }

        /// <summary>
        /// Calculates the amount that the user will pay in total (with tax).
        /// </summary>
        /// <returns>Decimal amount that user will pay in total.</returns>
        private decimal GetTotal()
        {
            return this.RawPrice + this.GetTaxCost();
        }
        
        /// <summary>
        /// Adds the given item to this order's list of items.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the given item is null.</exception>
        /// <param name="item">The item to add.</param>
        public void AddItem(Meal item)
        {
            if (item == null)
                { throw new ArgumentNullException("The given item cannot be null.", nameof(item)); }

            this.Items.Add(item);
        }
        #endregion Methods definition
    }
}
