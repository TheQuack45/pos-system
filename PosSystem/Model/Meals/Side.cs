﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a "side" meal.
    /// </summary>
    public class Side : Meal
    {
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Side;
        #endregion Static members definition
        #endregion Members definition

        public Side(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
    }
}
