﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    public class Combo : Meal
    {
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Combo;
        #endregion Static members definition

        #region Properties definition
        public new string Name
        {
            get { return this.GetName(); }
        }
        public List<MealPrototype> Components { get; private set; }
        // TODO: Where does price come from?
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public Combo(List<MealPrototype> components, string sizeName, MealPrototype prototype)
            : this(sizeName, prototype)
        {

        }

        private Combo(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
        #endregion Constructors definition

        #region Methods definition
        public string GetName()
        {
            // TODO: This.
            return "TODO: Combo name generation!";
        }
        #endregion Methods definition
    }
}
