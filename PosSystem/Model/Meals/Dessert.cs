﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a dessert.
    /// </summary>
    public class Dessert : Meal
    {
        // TODO: Desserts have some real crazy stuff going on.
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Dessert;
        #endregion Static members definition
        #endregion Members definition

        public Dessert(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
    }
}
