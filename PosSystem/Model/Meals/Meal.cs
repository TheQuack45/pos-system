﻿using PosSystem.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents an abstract base class for all meals, containing common information.
    /// </summary>
    public abstract class Meal : BindableBase
    {
        #region Members definition
        #region Properties definition
        private string _name;
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        private string _sizeName;
        /// <summary>
        /// The name of the size chosen. For example, "Medium".
        /// </summary>
        public string SizeName
        {
            get { return this._sizeName; }
            set { this.SetProperty(ref this._sizeName, value); }
        }

        private decimal _price;
        public decimal Price
        {
            get { return this._price; }
            set { this.SetProperty(ref this._price, value); }
        }

        private string _notes;
        /// <summary>
        /// Contains any extra notes a cashier may want to apply to the item.
        /// </summary>
        public string Notes
        {
            get { return this._notes; }
            set { this.SetProperty(ref this._notes, value); }
        }
        #endregion Properties definition

        #region Fields definition
        private readonly MealPrototype _prototype;
        #endregion Fields definition
        #endregion Members definition

        #region Constructors definition
        protected Meal(string sizeName, MealPrototype prototype, MealPrototype.MEAL_TYPE type)
        {
            if (type != prototype.Type)
                { throw new ArgumentException("The given meal type does not match that specified by the prototype."); }

            MealSize size = prototype.Sizes.Where(s => s.Name == sizeName)
                                           .Single();
            this.SizeName = size.Name;
            this.Price = size.Price;
            this.Name = prototype.Name;
            this._prototype = prototype;
        }
        #endregion Constructors definition
    }
}
