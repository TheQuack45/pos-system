﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a "basket" meal.
    /// Business logic: These are smaller version of Plates.
    /// </summary>
    public class Basket : Meal
    {
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Basket;
        #endregion Static members definition
        #endregion Members definition

        public Basket(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
    }
}
