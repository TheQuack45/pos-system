﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a beverage.
    /// </summary>
    public class Beverage : Meal
    {
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Beverage;
        #endregion Static members definition
        #endregion Members definition

        public Beverage(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
    }
}
