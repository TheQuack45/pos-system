﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a "plate" meal.
    /// Business logic: Should be served with french fries, onion rings, and cole slaw. Can be comboed with 1 or 2 other plates, and some can combo with clams.
    /// </summary>
    public class Plate : Meal
    {
        #region Members definition
        #region Static members definition
        private static readonly MealPrototype.MEAL_TYPE _validType = MealPrototype.MEAL_TYPE.Plate;
        #endregion Static members definition
        #endregion Members definition

        public Plate(string sizeName, MealPrototype prototype)
            : base(sizeName, prototype, _validType)
        {
        }
    }
}
