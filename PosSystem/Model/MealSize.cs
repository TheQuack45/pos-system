﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem.Model
{
    /// <summary>
    /// Represents a possible size for a meal, containing its name and price.
    /// </summary>
    public struct MealSize
    {
        public string Name { get; set; }
        public decimal Price { get; set; }

        public MealSize(string name, decimal price)
        {
            this.Name = name;
            this.Price = price;
        }
    }
}
