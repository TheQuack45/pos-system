﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PosSystem.Utility
{
    /// <summary>
    /// Represents a generic, synchronous implementation of the ICommand interface for command binding.
    /// </summary>
    /// <typeparam name="T">Type of the command's parameter.</typeparam>
    public class RelayCommand<T> : ICommand
    {
        #region Members definition
        private Action<T> _execute;
        private Predicate<T> _canExecute;

        public event EventHandler CanExecuteChanged;
        #endregion Members definition

        #region Constructors definition
        public RelayCommand(Action<T> execute, Predicate<T> canExecute)
        {
            this._execute = execute;
            this._canExecute = canExecute;
        }

        public RelayCommand(Action<T> execute)
            : this(execute, null)
        { }
        #endregion Constructors definition

        #region Methods definition
        public void Execute(T parameter)
        {
            this._execute(parameter);
        }

        public void Execute(object parameter)
        {
            this._execute((T)parameter);
        }

        public bool CanExecute(T parameter)
        {
            if (this._canExecute == null)
                { return true; }

            return this._canExecute(parameter);
        }

        public bool CanExecute(object parameter)
        {
            if (parameter == null)
                { return true; }

            return this.CanExecute((T)parameter);
        }
        #endregion Methods definition
    }

    public class RelayCommand : ICommand
    {
        #region Members definition
        private Action _execute;

        public event EventHandler CanExecuteChanged;
        #endregion Members definition

        #region Constuctors definition
        public RelayCommand(Action execute)
        {
            this._execute = execute;
        }
        #endregion Constructors definition

        #region Methods definition
        public void Execute()
        {
            this._execute();
        }

        public void Execute(object parameter)
        {
            this.Execute();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }
        #endregion Methods definition
    }
}
